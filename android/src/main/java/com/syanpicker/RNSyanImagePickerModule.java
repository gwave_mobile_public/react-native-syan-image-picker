
package com.syanpicker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;
import com.luck.picture.lib.animators.AnimationType;
//import com.luck.picture.lib.config.PictureSelectionConfig;
import com.luck.picture.lib.config.SelectorConfig;

import com.luck.picture.lib.config.SelectLimitType;
import com.luck.picture.lib.config.SelectModeConfig;
import com.luck.picture.lib.engine.CompressFileEngine;
import com.luck.picture.lib.engine.CropFileEngine;
import com.luck.picture.lib.engine.UriToFileTransformEngine;
import com.luck.picture.lib.interfaces.OnKeyValueResultCallbackListener;
import com.luck.picture.lib.interfaces.OnMediaEditInterceptListener;
import com.luck.picture.lib.interfaces.OnQueryFilterListener;
import com.luck.picture.lib.interfaces.OnSelectLimitTipsListener;
import com.luck.picture.lib.language.LanguageConfig;
import com.luck.picture.lib.style.PictureSelectorStyle;
import com.luck.picture.lib.utils.DateUtils;
import com.luck.picture.lib.utils.SandboxTransformUtils;
import com.luck.picture.lib.utils.ToastUtils;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.SelectMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.utils.PictureFileUtils;
import com.luck.picture.lib.utils.SdkVersionUtils;
import com.luck.pictureselector.ImageLoaderUtils;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropActivity;
import com.yalantis.ucrop.UCropImageEngine;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import top.zibin.luban.Luban;
import top.zibin.luban.OnNewCompressListener;
import top.zibin.luban.OnRenameListener;

public class RNSyanImagePickerModule extends ReactContextBaseJavaModule {

    private static String SY_SELECT_IMAGE_FAILED_CODE = "0"; // 失败时，Promise用到的code

    private final ReactApplicationContext reactContext;

    private List<LocalMedia> selectList = new ArrayList<>();

    private Callback mPickerCallback; // 保存回调

    private Promise mPickerPromise; // 保存Promise

    private ReadableMap cameraOptions; // 保存图片选择/相机选项

    public RNSyanImagePickerModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    @Override
    public String getName() {
        return "RNSyanImagePicker";
    }

    @ReactMethod
    public void showImagePicker(ReadableMap options, Callback callback) {
        Log.d("RNSyanImage", "showImagePicker options:" + options + ", cameraOptions:" + this.cameraOptions);
        this.cameraOptions = options;
        this.mPickerPromise = null;
        this.mPickerCallback = callback;
        this.openImagePicker();
    }

    @ReactMethod
    public void asyncShowImagePicker(ReadableMap options, Promise promise) {
        Log.d("RNSyanImage", "asyncShowImagePicker options:" + options + ", cameraOptions:" + this.cameraOptions);
        this.cameraOptions = options;
        this.mPickerCallback = null;
        this.mPickerPromise = promise;
        this.openImagePicker();
    }

    @ReactMethod
    public void openCamera(ReadableMap options, Callback callback) {
        this.cameraOptions = options;
        this.mPickerPromise = null;
        this.mPickerCallback = callback;
        this.openCamera();
    }

    @ReactMethod
    public void asyncOpenCamera(ReadableMap options, Promise promise) {
        this.cameraOptions = options;
        this.mPickerCallback = null;
        this.mPickerPromise = promise;
        this.openCamera();
    }

    /**
     * 缓存清除
     * 包括裁剪和压缩后的缓存，要在上传成功后调用，注意：需要系统sd卡权限
     */
    @ReactMethod
    public void deleteCache() {
        Activity currentActivity = getCurrentActivity();
        PictureFileUtils.deleteAllCacheDirFile(currentActivity);
    }

    /**
     * 移除选中的图片
     * index 要移除的图片下标
     */
    @ReactMethod
    public void removePhotoAtIndex(int index) {
        if (selectList != null && selectList.size() > index) {
            selectList.remove(index);
        }
    }

    /**
     * 移除所有选中的图片
     */
    @ReactMethod
    public void removeAllPhoto() {
        if (selectList != null) {
            //selectList.clear();
            selectList = null;
        }
    }

//    @ReactMethod
//    public void openVideo(ReadableMap options, Callback callback) {
//        this.cameraOptions = options;
//        this.mPickerPromise = null;
//        this.mPickerCallback = callback;
//        this.openVideo();
//    }

    @ReactMethod
    public void openVideoPicker(ReadableMap options, Callback callback) {
        this.cameraOptions = options;
        this.mPickerPromise = null;
        this.mPickerCallback = callback;
        this.openVideoPicker();
    }

    /**
     * 打开相册选择
     */
    private void openImagePicker() {
        int imageCount = this.cameraOptions.getInt("imageCount");
        boolean isCamera = this.cameraOptions.getBoolean("isCamera");
        boolean isCrop = this.cameraOptions.getBoolean("isCrop");
        boolean isGif = this.cameraOptions.getBoolean("isGif");
        boolean compress = this.cameraOptions.getBoolean("compress");
        int minimumCompressSize = this.cameraOptions.getInt("minimumCompressSize");
        // todo: custom style pending
//        boolean isWeChatStyle = this.cameraOptions.getBoolean("isWeChatStyle");
//        boolean showSelectedIndex = this.cameraOptions.getBoolean("showSelectedIndex");
        boolean compressFocusAlpha = this.cameraOptions.getBoolean("compressFocusAlpha");
        Log.d("RNSyanImage", "openImagePicker cameraOptions:" + this.cameraOptions);
//        int modeValue;
//        if (imageCount == 1) {
//            modeValue = 1;
//        } else {
//            modeValue = 2;
//        }

//        Boolean isAndroidQ = SdkVersionUtils.isQ();

        Activity currentActivity = getCurrentActivity();
        PictureSelector.create(currentActivity)
                .openGallery(SelectMimeType.ofImage())
                .setSelectorUIStyle(new PictureSelectorStyle())
                .setImageEngine(GlideEngine.createGlideEngine())
                .setCropEngine(isCrop ? new ImageFileCropEngine() : null)
                .setCompressEngine(compress ? new ImageFileCompressEngine(minimumCompressSize, compressFocusAlpha) : null)
                .setSandboxFileEngine(new MeSandboxFileEngine())
                .setCameraInterceptListener(null)
                .setRecordAudioInterceptListener(null)
                .setSelectLimitTipsListener(new MeOnSelectLimitTipsListener())
                .setEditMediaInterceptListener(isCrop ? new MeOnMediaEditInterceptListener(getSandboxPath(), buildOptions()) : null)
                .setPermissionDescriptionListener(null)
                .setPreviewInterceptListener(null)
                .setPermissionDeniedListener(null)
                .setAddBitmapWatermarkListener(null)
                .setVideoThumbnailListener(null)
                .isAutoVideoPlay(false)
                .isLoopAutoVideoPlay(false)
                .isPageSyncAlbumCount(true)
                .setCustomLoadingListener(null)
                .setQueryFilterListener(new OnQueryFilterListener() {
                    @Override
                    public boolean onFilter(LocalMedia media) {
                        return false;
                    }
                })
                //.setExtendLoaderEngine(getExtendLoaderEngine())
                .setInjectLayoutResourceListener(null)
                .setSelectionMode(imageCount > 1 ? SelectModeConfig.MULTIPLE : SelectModeConfig.SINGLE)
                .setLanguage(LanguageConfig.SYSTEM_LANGUAGE)
                .setQuerySortOrder("")
                .isDisplayTimeAxis(true)
                .isOnlyObtainSandboxDir(false)
                .isPageStrategy(true)
                .isOriginalControl(false)
                .isDisplayCamera(isCamera)
                .isOpenClickSound(false)
                .setSkipCropMimeType(getNotSupportCrop(isGif))
                .isFastSlidingSelect(true)
                //.setOutputCameraImageFileName("luck.jpeg")
                //.setOutputCameraVideoFileName("luck.mp4")
                .isWithSelectVideoImage(false)
                .isPreviewFullScreenMode(true)
                .isVideoPauseResumePlay(true)
                .isPreviewZoomEffect(true)
                .isPreviewImage(true)
                .setGridItemSelectAnimListener(null)
                .setSelectAnimListener(null)
                //.setQueryOnlyMimeType(PictureMimeType.ofGIF())
                .isMaxSelectEnabledMask(true)
                .isDirectReturnSingle(false)
                .setMaxSelectNum(imageCount)
                .setRecyclerAnimationMode(AnimationType.DEFAULT_ANIMATION)
                .isGif(isGif)
                .setSelectedData(selectList)
                .forResult(PictureConfig.CHOOSE_REQUEST); //结果回调onActivityResult code
    }

    /**
     * 打开相机
     */
    private void openCamera() {
        boolean isCrop = this.cameraOptions.getBoolean("isCrop");
        boolean compress = this.cameraOptions.getBoolean("compress");
        int minimumCompressSize = this.cameraOptions.getInt("minimumCompressSize");
        // todo: custom style pending
//        boolean isWeChatStyle = this.cameraOptions.getBoolean("isWeChatStyle");
//        boolean showSelectedIndex = this.cameraOptions.getBoolean("showSelectedIndex");
        boolean compressFocusAlpha = this.cameraOptions.getBoolean("compressFocusAlpha");

        Activity currentActivity = getCurrentActivity();
        PictureSelector.create(currentActivity)
                .openCamera(SelectMimeType.ofImage())
                .setCameraInterceptListener(null)
                .setRecordAudioInterceptListener(null)
                .setCropEngine(isCrop ? new ImageFileCropEngine() : null)
                .setCompressEngine(compress ? new ImageFileCompressEngine(minimumCompressSize, compressFocusAlpha) : null)
                .setAddBitmapWatermarkListener(null)
                .setVideoThumbnailListener(null)
                .setCustomLoadingListener(null)
                .setLanguage(LanguageConfig.SYSTEM_LANGUAGE)
                .setSandboxFileEngine(new MeSandboxFileEngine())
                .isOriginalControl(false)
                .setPermissionDescriptionListener(null)
                .setSelectedData(selectList)
                .forResultActivity(PictureConfig.CHOOSE_REQUEST);
    }

    /**
     * 拍摄视频
     */
//    private void openVideo() {
//        int quality = this.cameraOptions.getInt("quality");
//        int MaxSecond = this.cameraOptions.getInt("MaxSecond");
//        int MinSecond = this.cameraOptions.getInt("MinSecond");
//        int recordVideoSecond = this.cameraOptions.getInt("recordVideoSecond");
//        int imageCount = this.cameraOptions.getInt("imageCount");
//        Activity currentActivity = getCurrentActivity();
//        PictureSelector.create(currentActivity)
//                .openCamera(SelectMimeType.ofVideo())
//                .setCameraInterceptListener(null)
//                .setRecordAudioInterceptListener(null)
//                .setAddBitmapWatermarkListener(null)
//                .setVideoThumbnailListener(null)
//                .setCustomLoadingListener(null)
//                .setLanguage(LanguageConfig.SYSTEM_LANGUAGE)
//                .setSandboxFileEngine(new MeSandboxFileEngine())
//                .isOriginalControl(false)
//                .setPermissionDescriptionListener(null)
//                .setSelectedData(selectList)
//                .forResultActivity(PictureConfig.REQUEST_CAMERA);
//    }

    /**
     * 选择视频
     */
    private void openVideoPicker() {
        int quality = this.cameraOptions.getInt("quality");
        int MaxSecond = this.cameraOptions.getInt("MaxSecond");
        int MinSecond = this.cameraOptions.getInt("MinSecond");
        int recordVideoSecond = this.cameraOptions.getInt("recordVideoSecond");
        int videoCount = this.cameraOptions.getInt("imageCount");
        boolean isCamera = this.cameraOptions.getBoolean("allowTakeVideo");

        Activity currentActivity = getCurrentActivity();
        PictureSelector.create(currentActivity)
                .openGallery(SelectMimeType.ofVideo())
                .setSelectorUIStyle(new PictureSelectorStyle())
                .setImageEngine(GlideEngine.createGlideEngine())
                .setVideoPlayerEngine(null)
                .setSandboxFileEngine(new MeSandboxFileEngine())
                .setCameraInterceptListener(null)
                .setRecordAudioInterceptListener(null)
                .setSelectLimitTipsListener(new MeOnSelectLimitTipsListener())
                .setEditMediaInterceptListener(null)
                .setPermissionDescriptionListener(null)
                .setPreviewInterceptListener(null)
                .setPermissionDeniedListener(null)
                .setAddBitmapWatermarkListener(null)
                .setVideoThumbnailListener(null)
                .isAutoVideoPlay(false)
                .isLoopAutoVideoPlay(false)
                .isPageSyncAlbumCount(true)
                .setCustomLoadingListener(null)
                .setQueryFilterListener(new OnQueryFilterListener() {
                    @Override
                    public boolean onFilter(LocalMedia media) {
                        return false;
                    }
                })
                //.setExtendLoaderEngine(getExtendLoaderEngine())
                .setInjectLayoutResourceListener(null)
                .setSelectionMode(videoCount > 1 ? SelectModeConfig.MULTIPLE : SelectModeConfig.SINGLE)
                .setLanguage(LanguageConfig.SYSTEM_LANGUAGE)
                .setQuerySortOrder("")
                .isDisplayTimeAxis(true)
                .isOnlyObtainSandboxDir(false)
                .isPageStrategy(true)
                .isOriginalControl(false)
                .isDisplayCamera(isCamera)
                .isOpenClickSound(false)
                .isFastSlidingSelect(true)
                //.setOutputCameraImageFileName("luck.jpeg")
                //.setOutputCameraVideoFileName("luck.mp4")
                .isWithSelectVideoImage(false)
                .isPreviewFullScreenMode(true)
                .isVideoPauseResumePlay(true)
                .isPreviewZoomEffect(true)
                .isPreviewImage(true)
                .setGridItemSelectAnimListener(null)
                .setSelectAnimListener(null)
                //.setQueryOnlyMimeType(PictureMimeType.ofGIF())
                .isMaxSelectEnabledMask(true)
                .isDirectReturnSingle(false)
                .setMaxSelectNum(videoCount)
                .setRecyclerAnimationMode(AnimationType.DEFAULT_ANIMATION)
                .setSelectedData(selectList)
                .setVideoQuality(quality)
                .setSelectMaxDurationSecond(MaxSecond)
                .setSelectMinDurationSecond(MinSecond)
                .setRecordVideoMaxSecond(recordVideoSecond)
                .forResult(PictureConfig.REQUEST_CAMERA); //结果回调onActivityResult code
    }

    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, final Intent data) {
            if (resultCode == -1) {
                if (requestCode == PictureConfig.CHOOSE_REQUEST) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            List<LocalMedia> tmpSelectList = PictureSelector.obtainSelectorList(data);
                            onGetResult(tmpSelectList);
                        }
                    }).run();
                } else if (requestCode == PictureConfig.REQUEST_CAMERA) {
                    onGetVideoResult(data);
                }
            } else {
                invokeError(resultCode);
            }

        }
    };

    private void onGetVideoResult(Intent data) {
        List<LocalMedia> mVideoSelectList = PictureSelector.obtainSelectorList(data);
        if (cameraOptions != null) {
            boolean isRecordSelected = cameraOptions.getBoolean("isRecordSelected");
            if (!mVideoSelectList.isEmpty() && isRecordSelected) {
                selectList = mVideoSelectList;
            }
            WritableArray videoList = new WritableNativeArray();

            for (LocalMedia media : mVideoSelectList) {
                if (TextUtils.isEmpty(media.getPath())) {
                    continue;
                }

                WritableMap videoMap = new WritableNativeMap();

                boolean isAndroidQ = SdkVersionUtils.isQ();
                boolean isAndroidR = SdkVersionUtils.isR();
                String filePath = media.getPath();
                // todo: check getAndroidQToPath()
//                if (isAndroidQ){
//                   filePath = media.getAndroidQToPath();
//                }
               if (isAndroidR){
                   filePath = media.getRealPath();
               }
                filePath = getOriginalFilepath(filePath);
                videoMap.putString("uri", "file://" + filePath);
                videoMap.putString("coverUri", "file://" + this.getVideoCover(filePath));
                videoMap.putString("fileName", new File(media.getPath()).getName());
                videoMap.putDouble("size", new File(media.getPath()).length());
                videoMap.putDouble("duration", media.getDuration() / 1000.00);
                videoMap.putInt("width", media.getWidth());
                videoMap.putInt("height", media.getHeight());
                videoMap.putString("type", "video");
                videoMap.putString("mime", media.getMimeType());
                videoList.pushMap(videoMap);
            }

            invokeSuccessWithResult(videoList);
        }
    }
    private String getOriginalFilepath(String filepath) {// 华为等部分手机只返回 content://media/external/video/media/39106
        String originalFilepath = filepath;
        try {
            Uri uri = Uri.parse(filepath);
            if (uri.getScheme().equals("content")) {
                Cursor cursor = reactContext.getContentResolver().query(uri, null, null, null, null);
                if (cursor.moveToFirst()) {
                    originalFilepath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));
                }
                cursor.close();
            }
        } catch (Exception ignored) {
        }
        if(originalFilepath == null){// 容错
            originalFilepath = filepath;
        }
        return originalFilepath;
    }

    private void onGetResult(List<LocalMedia> tmpSelectList) {
        if (cameraOptions != null) {
            boolean isRecordSelected = cameraOptions.getBoolean("isRecordSelected");
            if (!tmpSelectList.isEmpty() && isRecordSelected) {
                selectList = tmpSelectList;
            }

            WritableArray imageList = new WritableNativeArray();
            boolean enableBase64 = cameraOptions.getBoolean("enableBase64");

            for (LocalMedia media : tmpSelectList) {
                imageList.pushMap(getImageResult(media, enableBase64));
            }
            invokeSuccessWithResult(imageList);
        }
    }

    private WritableMap getImageResult(LocalMedia media, Boolean enableBase64) {
        WritableMap imageMap = new WritableNativeMap();
        String path = media.getPath();

        if (media.isCompressed() || media.isCut()) {
            path = media.getCompressPath();
        }

        if (media.isCut()) {
            path = media.getCutPath();
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Boolean isAndroidR = SdkVersionUtils.isR();
        String fileRealPath = path;
        int size = (int) new File(path).length();
        String uri = "file://" + path;

        if (isAndroidR){
             fileRealPath = media.getRealPath();
        }
        try {
            BitmapFactory.decodeFile(path, options);
            if (path != null && options.outWidth == 0 && options.outHeight == 0) {
                BitmapFactory.decodeFile(fileRealPath, options);
            }
        } catch (Exception e) {
            Log.d("LogD:e", e.toString());
        }

        if (size == 0) {
            size = (int) new File(fileRealPath).length();
            uri = "file://" + fileRealPath;
        }
        imageMap.putString("uri", uri);
        imageMap.putInt("size", size);
        imageMap.putDouble("width", options.outWidth);
        imageMap.putDouble("height", options.outHeight);
        imageMap.putString("type", "image");
        imageMap.putString("original_uri", "file://" + media.getPath());

        if (enableBase64) {
            String encodeString = getBase64StringFromFile(path);
            imageMap.putString("base64", encodeString);
        }

        return imageMap;
    }

    /**
     * 获取图片base64编码字符串
     *
     * @param absoluteFilePath 文件路径
     * @return base64字符串
     */
    private String getBase64StringFromFile(String absoluteFilePath) {
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(new File(absoluteFilePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        byte[] bytes;
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        bytes = output.toByteArray();
        if(absoluteFilePath.toLowerCase().endsWith("png")){
          return "data:image/png;base64," + Base64.encodeToString(bytes, Base64.NO_WRAP);
        }
        return "data:image/jpeg;base64," + Base64.encodeToString(bytes, Base64.NO_WRAP);
    }


    /**
     * 获取视频封面图片
     * @param videoPath 视频地址
     */
    private String getVideoCover(String videoPath) {
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(videoPath);
            Bitmap bitmap = retriever.getFrameAtTime();
            FileOutputStream outStream = null;
            final String uuid = "thumb-" + UUID.randomUUID().toString();
            final String localThumb = reactContext.getExternalCacheDir().getAbsolutePath() + "/" + uuid + ".jpg";
            outStream = new FileOutputStream(new File(localThumb));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, outStream);
            outStream.close();
            retriever.release();

            return localThumb;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception err) {
            err.printStackTrace();
        }

        return null;
    }

    /**
     * 选择照片成功时触发
     *
     * @param imageList 图片数组
     */
    private void invokeSuccessWithResult(WritableArray imageList) {
        if (this.mPickerCallback != null) {
            this.mPickerCallback.invoke(null, imageList);
            this.mPickerCallback = null;
        } else if (this.mPickerPromise != null) {
            this.mPickerPromise.resolve(imageList);
        }
    }

    /**
     * 取消选择时触发
     */
    private void invokeError(int resultCode) {
        String message = "取消";
        if (resultCode != 0) {
            message = String.valueOf(resultCode);
        }
        if (this.mPickerCallback != null) {
            this.mPickerCallback.invoke(message);
            this.mPickerCallback = null;
        } else if (this.mPickerPromise != null) {
            this.mPickerPromise.reject(SY_SELECT_IMAGE_FAILED_CODE, message);
        }
    }

    /**
     * 配制UCrop，可根据需求自我扩展
     *
     * @return
     */
    private UCrop.Options buildOptions() {
        Activity currentActivity = getCurrentActivity();
        UCrop.Options options = new UCrop.Options();
        if(currentActivity == null) {
            return options;
        }
        int CropW = this.cameraOptions.getInt("CropW");
        int CropH = this.cameraOptions.getInt("CropH");
        boolean isCropFrameAutoFitImage = this.cameraOptions.getBoolean("isCropFrameAutoFitImage");
        if (isCropFrameAutoFitImage) {
            CropW = -1;
            CropH = -1;
        }
        Log.d("RNSyanImage", "UCrop buildOptions cameraOptions:" + this.cameraOptions);
        boolean showCropCircle = this.cameraOptions.getBoolean("showCropCircle");
        boolean showCropFrame = this.cameraOptions.getBoolean("showCropFrame");
        boolean showCropGrid = this.cameraOptions.getBoolean("showCropGrid");
        boolean freeStyleCropEnabled = this.cameraOptions.getBoolean("freeStyleCropEnabled");
        boolean isGif = this.cameraOptions.getBoolean("isGif");
        int quality = this.cameraOptions.getInt("quality");
        boolean rotateEnabled = this.cameraOptions.getBoolean("rotateEnabled");
        boolean scaleEnabled = this.cameraOptions.getBoolean("scaleEnabled");
        boolean isHideBottomControls = this.cameraOptions.getBoolean("isHideBottomControls");
        options.setHideBottomControls(isHideBottomControls);
        options.setFreeStyleCropEnabled(freeStyleCropEnabled);
        options.setShowCropFrame(showCropFrame);
        options.setShowCropGrid(showCropGrid);
        options.setCircleDimmedLayer(showCropCircle);
        options.withAspectRatio((CropW > 0) ? CropW : -1, (CropH > 0) ? CropH : -1);
        options.setCropOutputPathDir(getSandboxPath());
        options.isCropDragSmoothToCenter(false);
        options.setSkipCropMimeType(getNotSupportCrop(isGif));
        options.isForbidCropGifWebp(!isGif);
        options.isForbidSkipMultipleCrop(false);
        options.setMaxScaleMultiplier(100);
        options.setCompressionQuality(quality);
        if (!rotateEnabled) {
            options.setAllowedGestures(UCropActivity.SCALE, UCropActivity.SCALE, UCropActivity.ALL);
            if (!scaleEnabled) {
                options.setAllowedGestures(UCropActivity.NONE, UCropActivity.NONE, UCropActivity.NONE);
            }
        }
        options.setStatusBarColor(ContextCompat.getColor(currentActivity, R.color.ps_color_grey));
        options.setToolbarColor(ContextCompat.getColor(currentActivity, R.color.ps_color_grey));
        options.setToolbarWidgetColor(ContextCompat.getColor(currentActivity, R.color.ps_color_white));
        return options;
    }

    /**
     * 自定义裁剪
     */
    private class ImageFileCropEngine implements CropFileEngine {

        @Override
        public void onStartCrop(Fragment fragment, Uri srcUri, Uri destinationUri, ArrayList<String> dataSource, int requestCode) {
            UCrop.Options options = buildOptions();
            UCrop uCrop = UCrop.of(srcUri, destinationUri, dataSource);
            uCrop.withOptions(options);
            uCrop.setImageEngine(new UCropImageEngine() {
                @Override
                public void loadImage(Context context, String url, ImageView imageView) {
                    if (!ImageLoaderUtils.assertValidRequest(context)) {
                        return;
                    }
                    Glide.with(context).load(url).override(180, 180).into(imageView);
                }

                @Override
                public void loadImage(Context context, Uri url, int maxWidth, int maxHeight, OnCallbackListener<Bitmap> call) {
                    Glide.with(context).asBitmap().load(url).override(maxWidth, maxHeight).into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            if (call != null) {
                                call.onCall(resource);
                            }
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                            if (call != null) {
                                call.onCall(null);
                            }
                        }
                    });
                }
            });
            uCrop.start(fragment.requireActivity(), fragment, requestCode);
        }
    }

    /**
     * 自定义压缩
     */
    private static class ImageFileCompressEngine implements CompressFileEngine {
        private final int minCompressSize;
        private final boolean compressFocusAlpha;

        public ImageFileCompressEngine(int minCompressSize, boolean compressFocusAlpha) {
            this.minCompressSize = minCompressSize;
            this.compressFocusAlpha =compressFocusAlpha;
        }

        @Override
        public void onStartCompress(Context context, ArrayList<Uri> source, OnKeyValueResultCallbackListener call) {
            Luban.with(context).load(source).ignoreBy(minCompressSize).setFocusAlpha(compressFocusAlpha).setRenameListener(new OnRenameListener() {
                @Override
                public String rename(String filePath) {
                    int indexOf = filePath.lastIndexOf(".");
                    String postfix = indexOf != -1 ? filePath.substring(indexOf) : ".jpg";
                    return DateUtils.getCreateFileName("CMP_") + postfix;
                }
            }).setCompressListener(new OnNewCompressListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onSuccess(String source, File compressFile) {
                    if (call != null) {
                        call.onCallback(source, compressFile.getAbsolutePath());
                    }
                }

                @Override
                public void onError(String source, Throwable e) {
                    if (call != null) {
                        call.onCallback(source, null);
                    }
                }
            }).launch();
        }
    }

    /**
     * 自定义沙盒文件处理
     */
    private static class MeSandboxFileEngine implements UriToFileTransformEngine {

        @Override
        public void onUriToFileAsyncTransform(Context context, String srcPath, String mineType, OnKeyValueResultCallbackListener call) {
            if (call != null) {
                call.onCallback(srcPath, SandboxTransformUtils.copyPathToSandbox(context, srcPath, mineType));
            }
        }
    }

    /**
     * 创建自定义输出目录
     *
     * @return
     */
    private String getSandboxPath() {
        Activity currentActivity = getCurrentActivity();
        try {
            File externalFilesDir = currentActivity.getExternalFilesDir("");
            File customFile = new File(externalFilesDir.getAbsolutePath(), "Sandbox");
            if (!customFile.exists()) {
                customFile.mkdirs();
            }
            return customFile.getAbsolutePath() + File.separator;
        } catch (NullPointerException e) {
        }
        return "";
    }

    private String[] getNotSupportCrop(boolean isGif) {
        if (isGif) {
            return null;
        }
        return new String[]{PictureMimeType.ofGIF(), PictureMimeType.ofWEBP()};
    }

//    /**
//     * 拦截自定义提示
//     */
//    private static class MeOnSelectLimitTipsListener implements OnSelectLimitTipsListener {
//
//        @Override
//        public boolean onSelectLimitTips(Context context, SelectorConfig config, int limitType) {
//            if (limitType == SelectLimitType.SELECT_NOT_SUPPORT_SELECT_LIMIT) {
//                ToastUtils.showToast(context, "暂不支持的选择类型");
//                return true;
//            }
//            return false;
//        }
//    }
    /**
     * 拦截自定义提示
     */
    private static class MeOnSelectLimitTipsListener implements OnSelectLimitTipsListener {

        @Override
        public boolean onSelectLimitTips(Context context, @Nullable LocalMedia media, SelectorConfig config, int limitType) {
            if (limitType == SelectLimitType.SELECT_MIN_SELECT_LIMIT) {
                ToastUtils.showToast(context, "图片最少不能低于" + config.minSelectNum + "张");
                return true;
            } else if (limitType == SelectLimitType.SELECT_MIN_VIDEO_SELECT_LIMIT) {
                ToastUtils.showToast(context, "视频最少不能低于" + config.minVideoSelectNum + "个");
                return true;
            } else if (limitType == SelectLimitType.SELECT_MIN_AUDIO_SELECT_LIMIT) {
                ToastUtils.showToast(context, "音频最少不能低于" + config.minAudioSelectNum + "个");
                return true;
            }
            return false;
        }
    }
    /**
     * 自定义编辑
     */
    private static class MeOnMediaEditInterceptListener implements OnMediaEditInterceptListener {
        private final String outputCropPath;
        private final UCrop.Options options;

        public MeOnMediaEditInterceptListener(String outputCropPath, UCrop.Options options) {
            this.outputCropPath = outputCropPath;
            this.options = options;
        }

        @Override
        public void onStartMediaEdit(Fragment fragment, LocalMedia currentLocalMedia, int requestCode) {
            String currentEditPath = currentLocalMedia.getAvailablePath();
            Uri inputUri = PictureMimeType.isContent(currentEditPath)
                    ? Uri.parse(currentEditPath) : Uri.fromFile(new File(currentEditPath));
            Uri destinationUri = Uri.fromFile(
                    new File(outputCropPath, DateUtils.getCreateFileName("CROP_") + ".jpeg"));
            UCrop uCrop = UCrop.of(inputUri, destinationUri);
            options.setHideBottomControls(false);
            uCrop.withOptions(options);
            uCrop.setImageEngine(new UCropImageEngine() {
                @Override
                public void loadImage(Context context, String url, ImageView imageView) {
                    if (!com.luck.pictureselector.ImageLoaderUtils.assertValidRequest(context)) {
                        return;
                    }
                    Glide.with(context).load(url).override(180, 180).into(imageView);
                }

                @Override
                public void loadImage(Context context, Uri url, int maxWidth, int maxHeight, OnCallbackListener<Bitmap> call) {
                    Glide.with(context).asBitmap().load(url).override(maxWidth, maxHeight).into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            if (call != null) {
                                call.onCall(resource);
                            }
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                            if (call != null) {
                                call.onCall(null);
                            }
                        }
                    });
                }
            });
            uCrop.startEdit(fragment.requireActivity(), fragment, requestCode);
        }
    }
}
