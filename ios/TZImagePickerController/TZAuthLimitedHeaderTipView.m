//
//  TZAuthLimitedHeaderTipView.m
//  TZImagePickerController
//
//  Created by Henry on 2022/8/29.
//  Copyright © 2022 谭真. All rights reserved.
//

#import "TZAuthLimitedHeaderTipView.h"
#import "TZImagePickerController.h"

@interface TZAuthLimitedHeaderTipView()
@property (nonatomic,strong) UILabel *tipLabel;
@property (nonatomic,strong) UIButton *settingButton;
@end

@implementation TZAuthLimitedHeaderTipView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews {
    self.backgroundColor = [UIColor colorWithRed:250 / 255.0f green:250 / 255.0f blue:250 / 255.0f alpha:1.0f];
    [self addSubview:self.tipLabel];
    [self addSubview:self.settingButton];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
    CGFloat pixelRatio = screenW / 750.0f;
    CGFloat paddingHorizontal = 30 * pixelRatio;
    CGFloat settingButtonWidth = 105 * pixelRatio;
    self.tipLabel.frame = CGRectMake(paddingHorizontal, 0, self.bounds.size.width - paddingHorizontal * 3 - settingButtonWidth, self.bounds.size.height);
    CGFloat settingButtonHeight = 44 * pixelRatio;
    self.settingButton.frame = CGRectMake(self.bounds.size.width - paddingHorizontal - settingButtonWidth, (self.bounds.size.height - settingButtonHeight) / 2.0f, settingButtonWidth, settingButtonHeight);
}

#pragma mark - Getter

- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.numberOfLines = 0;
        _tipLabel.font = [UIFont systemFontOfSize:14];
        _tipLabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    }
    return _tipLabel;
}

- (UIButton *)settingButton {
    if (!_settingButton) {
        CGFloat screenW = [UIScreen mainScreen].bounds.size.width;
        CGFloat pixelRatio = screenW / 750.0f;
        _settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _settingButton.layer.cornerRadius = 22 * pixelRatio;
        _settingButton.backgroundColor = [UIColor colorWithRed:111 / 255.0f green:64 / 255.0f blue:238 / 255.0f alpha:1.0f];
        [_settingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _settingButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [_settingButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _settingButton;
}

- (void)settingButtonClicked {
    if (self.settingButtonDidClicked) {
        self.settingButtonDidClicked();
    }
}

#pragma mark -Setter

- (void)setAuthLimitedTipTitle:(NSString *)authLimitedTipTitle {
    _authLimitedTipTitle = authLimitedTipTitle;
    _tipLabel.text = authLimitedTipTitle;
}

- (void)setAuthLimitedTipSetting:(NSString *)authLimitedTipSetting {
    _authLimitedTipSetting = authLimitedTipSetting;
    [_settingButton setTitle:authLimitedTipSetting forState:UIControlStateNormal];
}

@end
